declare module 'vue3-twemoji-picker-final'

type AvaItem = {
  img: string
  isSelect: boolean
}

type UserInfo = {
  name: string
  userName: string
  token: string
  avator: string
}

type ChatTarget = {
  avator: string
  name: string
  socketId: string
  token: string
  userName: string
}