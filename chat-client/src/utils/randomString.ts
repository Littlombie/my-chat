export function randomString(len:number) {
  const chats = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
  let tempLen = chats.length,
      tempStr = ''
  for (let i = 0; i < len; i++) {
    tempStr += chats.charAt(Math.floor(Math.random() * tempLen))
  }
  return tempStr
}