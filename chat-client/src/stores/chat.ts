import { ref, computed, reactive } from 'vue'
import { defineStore } from 'pinia'
import { useUserStore } from './user'

interface OnlineUser {
  chatName?: string
  userList?: UserInfo[]
}


export const useChatStore = defineStore('ChatStore', () => {
  const useUser = useUserStore()

  const chat = reactive<{
    socket: any
    userOnlineList: ChatTarget[]
    chatMessageList: {}
  }>(
    {
      socket: null,
      userOnlineList: [],
      chatMessageList: {}
    }
  )
  
  //  获取用户信息
  const userInfo = computed(() => useUser.userInfo) 

  // const getUserOnlineList = computed(() => userOnlineList.value) 

  // 初始化socket
  const initSocket = () => {

    // 使用用户token 登录
    // socket = io(chatUrl, {
    //   auth: {
    //     token:  userInfo.value.token,
    //     ...userInfo.value
    //   }
    // })

    // 连接socket
    // socket.on("connect", () => {
    //   const engine = socket.io.engine;
    //   // console.log(engine.transport.query?.sid); 
    // });

    // socket.on('online', (list) => {
    //   userOnlineList.value = list
    //   console.log('在线列表', userOnlineList.value)
    // }) 

    // 加入房间成功
    // socket.on('joinSuccess', (room, data) => {
    //   console.log('加入房间成功， 用户列表', room, data)
    //   // UsersOnline = {
    //   //   chatName: room,
    //   //   userList: data
    //   // }
    // })

    // // 接收到的消息
    // socket.on('reviceMsg', (data) => {
    //   console.log('接收到的消息：', data)
    //   appendData(data)
    // })
    
    // // 检测断开连接
    // socket.on("disconnect", () => {
    //   console.log("连接已断开")
    // })
  }

  return {
    chat,
    initSocket,
    // chatMessageList,
  }
})