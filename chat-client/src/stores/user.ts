import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import  storage from '@/utils/storage'

export const useUserStore = defineStore('userStore', () => {
  const userInfo = ref<UserInfo>({
    name: '',
    userName: '',
    avator: '',
    token: ''
  })
  const chatTargetUser = ref<ChatTarget>(null)

  const getUserInfo = computed(() => userInfo.value)

  const setUserInfo = (info:UserInfo | null) => {
    userInfo.value = info as UserInfo
    console.log(userInfo.value)
    if (userInfo.value === null) {
      const session = storage()
      session.get('userStore') && session.remove('userStore')
    }
  }

  const setChatUser = ((user:ChatTarget) => {
    console.log('targetInfo', user)
    chatTargetUser.value = user
  })

  return { userInfo, getUserInfo, setUserInfo, chatTargetUser, setChatUser}
}, {
  persist: {
    storage: sessionStorage,
  },
})
