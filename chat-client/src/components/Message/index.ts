import MyMessage from './index.vue'
import { createVNode, render } from 'vue'


const div = document.createElement('div')
div.setAttribute('class', 'message-container')
document.body.appendChild(div)


let timer:any = null
export default ({type, text}: {type:string, text: string}) => {
  const vnode = createVNode(MyMessage, {type, text, onClose: () => {
    render(null, div)
    clearTimeout(timer)
  }})
  
  // console.log(vnode)
  render(vnode, div)
  
  clearTimeout(timer)
  timer = setTimeout(() => {
    render(null, div)
  }, 3000);
}