import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home/index.vue'
import Layout from '@/layout/index.vue'
import { useUserStore } from '@/stores/user'
// import Chat from ;

const routes = [
  {
    path: '/',
    redirect: '/home',
    component: Layout,
    children: [
      {
        path: '/home',
        name: 'Home',
        component: Home
      },
      {
        path: '/chat',
        name: 'Chat',
        component: () => import('@/views/Chat/index.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  // console.log({from}, {to})
  const store = useUserStore()
  let hasToName = routes[0].children.some(item => item.name === to.name)
  if (!hasToName) next({name: 'Home'})
  if (!store.userInfo && to.name !== 'Home') next({name: 'Home'})
  else next()
})

export default router
