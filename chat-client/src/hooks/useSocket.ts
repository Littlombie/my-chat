import { io } from 'socket.io-client'
import { useUserStore  } from '@/stores/user'
import { useChatStore } from '@/stores/chat'

const chatUrl = import.meta.env.VITE_BASE_API || 'http://localhost:9090'

const { chat } = useChatStore()
const { getUserInfo } = useUserStore ()


export const initSocket = () => {

  console.log(import.meta.env, {chatUrl})
  // 使用用户token 登录
  chat.socket = io(chatUrl, {
    auth: {
      token: getUserInfo.token,
      ...getUserInfo
    }
  })

  // 接收在线
  chat.socket.on('online', (list) => {
    chat.userOnlineList = list
    console.log('在线列表', chat.userOnlineList)
  })

  // 加入房间成功
  chat.socket.on('joinSuccess', (room, data) => {
    console.log('加入房间成功， 用户列表', room, data)
    // UsersOnline = {
    //   chatName: room,
    //   userList: data
    // }
  })

  // 接收到的消息
  chat.socket.on('reviceMsg', (data) => {
    console.log('接收到的消息：', data)
    appendData(data)
  })
  
  // 检测断开连接
  chat.socket.on("disconnect", () => {
    console.log("连接已断开")
  })
} 

  // 发送： 加入房间
export  const jionRoom = (roomName:string) => {
  if (!chat.socket) {
    console.log("连接已断开")
    initSocket()
    return
  }
  console.log('toJion')
  const user = {
    name: getUserInfo.name,
    avator: getUserInfo.avator
  }
  chat.socket.emit('joinRoom', roomName, user)
}

  // 发送： 发送消息
export  const sendMsg = (data) => {
  if (!chat.socket) {
    return
  }
  chat.socket.emit('snedMsg', data)
}

  // 断开连接
export  const disConnect = () => {
  console.log('断开')
  chat.socket.disconnect('客户端断开连接', getUserInfo)
}

function appendData (data) {
  !chat.chatMessageList[getUserInfo.userName] && (chat.chatMessageList[getUserInfo.userName] = [])
  const fromUser = chat.userOnlineList.filter(user => user.userName === data.fromUserName)
  if (fromUser.length) {
    const fromMsg = {
      fromAvator: fromUser[0].avator,
      fromName: fromUser[0].name,
      msgT: 1,
      ...data
    }
    chat.chatMessageList[getUserInfo.userName].push(fromMsg)
  }
}