import MyMessage from '@/components/Message/index'

export function useMessage() {

  const SendMessage =(type: string, text: any) => {
    MyMessage({
      type,
      text
    })
  }
  const error = (text: any) => {
    MyMessage({
      type: 'error',
      text
    })
  }
  const info = (text: any) => {
    MyMessage({
      type: 'info',
      text
    })
  }
  const success = (text: any) => {
    MyMessage({
      type: 'success',
      text
    })
  }
  const warning = (text: any) => {
    MyMessage({
      type: 'warning',
      text
    })
  }
  return {
    SendMessage,
    error,
    info,
    success,
    warning
  }
}