import { fileURLToPath, URL } from 'node:url'

import { UserConfigExport, ConfigEnv, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'


// console.log(import.meta)
// https://vitejs.dev/config/
/** 当前执行node命令时文件夹的地址（工作目录） */
const root: string = process.cwd();

export default ({ command, mode }: ConfigEnv): UserConfigExport => {
  const ENV = loadEnv(mode, root)
  // console.log(ENV.VITE_ENV)
  return {
    plugins: [
      vue(),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      host: '0.0.0.0',
      port: 9091
    },
    build: {
      minify: 'terser', // 使用terser 方式压缩代码
      outDir: `dist-${ENV.VITE_ENV}`, // 打包后的文件目录
      terserOptions: {
        compress: { // 打包去除打印和debugger
          drop_console: ENV.VITE_ENV === 'development' ? false : true,
          drop_debugger: true,
        }
      },
    },
  }
}
