/** @type {import('tailwindcss').Config} */

const colors = require('tailwindcss/colors')

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  css: ['./src/assets/tailwind.css'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
  screens: {
    xs: "614px",
    sm: "1002px",
    md: "1022px",
    lg: "1092px",
    xl: "1280px",
    xl2: "1536px"
  },
    extend: {
      colors: {
        bookmark: {
          white: {

          },
          dark: {
            descContent: "#444545"
          }
        }
      } 
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}