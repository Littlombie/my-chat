# MyChat 聊天室
> 本项目是学习`Websocket`的及时聊天练习项目。主要使用`socket.io`实现连线数据传输。项目还在更新中……

## 功能介绍
本项目目前只实现了基本的用户登录选择头像输入用户名连接socket， 查看在线用户，点击用户开始一对一聊天，目前聊天内容仅支持文字和表情包。

## 后续更新
* 实现后台连接数据库管理用户、消息、资源
* 共享聊天室
* 可以发送图片、视频
* ……

## 演示地址
[聊天室](http://112.74.167.195:9091/)

## 技术选型
### 后端
* `socket.io`:  实现服务端的与客户端的数据连接
* `express`： 服务搭建初始化

### 前端
* `vue3 + ts + vue-router + pinia`：前端的基本框架及路由数据共享
* `pinia-plugin-persistedstate`： 实现数据持续化
* `tailwindcss + headlessui + heroicons`： 页面的样式、图标 

## 项目启动
### 后端
进入后端页面
```
cd socket
```
安装依赖
```
pnpm i
```
执行运行项目 `nodemon app.js`
```
pnpm dev
```
### 前端页面
进入前端页面
```
cd chat-client
```
安装依赖
```
pnpm i
```
运行项目
```
pnpm dev
```
打包项目
```
pnpm build:prod // 生产环境
pnpm build:dev // 开发环境
```
