import express from 'express'
import { createServer } from 'http'
import { Server } from 'socket.io'

let userList = []
const app = express()
const httpServer = createServer(app)
const io = new Server(httpServer, {
  cors: {
    origin: '*' // http://localhost:9091
  }
})

io.on('connection', (socket) => {
  let clientAuth = socket.handshake.auth
  if (!clientAuth.userName) return
  // console.log('socket已连接', socket.id, clientAuth)
  const sameUser = userList.find(({userName}) => userName === clientAuth.userName)
  if (sameUser) {
    sameUser.socketId = socket.id
  } else {
    let userInfo = {
      socketId: socket.id,
      ...clientAuth
    }
    userList.unshift(userInfo)
  }

  // userList = [...new Set(userList)]
  // console.log('在线列表', userList)
  // console.log('----------------------')

  io.emit('online', userList)

  // 加入房间
  socket.on('joinRoom', async (room, data) => {
    // console.log('加入房间:', room, data)
    userList.unshift(data)
    // 进入
    // socket.join("room1");
    socket.emit("joinSuccess", room, userList)
  })

  // 接收消息
  socket.on('snedMsg', (data) => {
    // console.log('接收消息', data)
    if (!data) return
    // 通过socketid找到发送的个人
    const targetSocket = io.sockets.sockets.get(data.targetId)
    const toUser = userList.find(user => user.socketId === data.targetId)
    // console.log('匹配的用户',toUser, data.targetId)
    if (toUser) {
      // 传送给客户端的信息内容
      const sendData = {
        fromUserName: data.fromUserName,
        toUserName: toUser.userName,
        msg: data.msg,
        dateTime: new Date().getTime()
      }
      targetSocket.emit('reviceMsg', sendData)
      // socket.to().emit('reviceMsg', data)
    }
  })

  // 

  // 断开连接
  socket.on('disconnect', (reason, data) => {
    // not triggered
    // console.log('检测到客户端断开连接！', reason, data)
    // console.log('now socket Info', socket.id, socket.handshake.auth)
    userList = userList.filter(item => item.socketId !== socket.id)
    // console.log('now UserList', userList)
  });

})

// io.use((socket, next) => {
//   setTimeout(() => {
//     // next is called after the client disconnection
//     next();
//   }, 1000);

// });


httpServer.listen(9090)
console.log('lesten to http://localhost:9090')
console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')